FROM docker.io/library/ubuntu:18.04

ENV  DEBIAN_FRONTEND noninteractive

RUN apt-get update
RUN apt-get install -y --no-install-recommends               \
    apt-utils build-essential autoconf automake  libtool-bin \
    zip unzip bzip2 xz-utils gzip zstd sudo cmake tree       \
    vim less file gawk lsof wget curl ssh iputils-ping       \
    ca-certificates git git-gui gitk git-lfs telnet          \
    libz-dev libbz2-dev liblz4-dev liblzma-dev libssl-dev    \
    libjsoncpp-dev         libgflags-dev                     \
    libwebsocketpp-dev     python3  python3-dev python-dev   \
    libboost-dev           libboost-system-dev               \
    libgeographic-dev      uuid-dev patchelf                 \
    libgl1-mesa-dev                                          \
    libglew-dev libsdl2-dev libsdl2-image-dev                \
    libglm-dev             libfreetype6-dev                  \ 
    libtheora0 libva2 libva-dev libvdpau1 libhdf5-dev        \
    libgtk2.0-0 libnuma-dev bc psmisc libcanberra-gtk-module
WORKDIR /opt
COPY installers /opt/installers

# install pip
RUN bash /opt/installers/install_pip.sh
# install airosrt
RUN bash /opt/installers/install_airosrt.sh
# install cuda
RUN bash /opt/installers/install_cuda.sh
# install tensorrt
RUN bash /opt/installers/install_tensorrt.sh
# install paddle
RUN bash /opt/installers/install_paddle.sh
# install bazel-5.0.0
RUN bash /opt/installers/install_bazel.sh
# install boost
RUN bash /opt/installers/install_boost.sh
# install dahuasdk
RUN bash /opt/installers/install_dahuasdk.sh
# install hikvisionsdk
RUN bash /opt/installers/install_hikvisionsdk.sh
# install ffmpeg
RUN bash /opt/installers/install_ffmpeg.sh
# install glew
RUN bash /opt/installers/install_glew.sh
# install glfw
RUN bash /opt/installers/install_glfw.sh
# install opencv
RUN bash /opt/installers/install_opencv.sh
# install xml
RUN bash /opt/installers/install_xml.sh
# install yaml
RUN bash /opt/installers/install_yaml.sh
# install paho
RUN bash /opt/installers/install_paho.sh
# install atlas
RUN bash /opt/installers/install_atlas.sh
# install abseil
RUN bash /opt/installers/install_abseil.sh
# install asmwrapper
RUN bash /opt/installers/install_asnwrapper.sh
# install fastrtps
RUN bash /opt/installers/install_fastrtps.sh
# install video_decoder
RUN bash /opt/installers/install_videodecoder.sh
# create SN
RUN mkdir -p /etc/v2x && echo RSCU_DEFAULT_SN > /etc/v2x/SN

# clean
RUN rm -rf /opt/installers

RUN /sbin/ldconfig
