#!/usr/bin/env bash

set -e

source /opt/installers/install_base.sh

function install_cuda {
    local url="https://zhilu.bj.bcebos.com/cuda.tar.gz"
    local sha256="798b9f21531c4a9f64c2277d7cc28406839234ac217fc64659b945e7dc2434cc"
    [ -z "${url}" ] && exit
    pushd /opt/ >/dev/null
        wget -c ${url}
        if check_sha256 "cuda.tar.gz" "${sha256}"; then
            tar -zxf cuda.tar.gz
        else
            echo "check_sha256 failed: cuda.tar.gz"
        fi
        rm cuda.tar.gz
    popd >/dev/null
}

function main {
    install_cuda
}

main "$@"
