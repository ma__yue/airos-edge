#!/usr/bin/env bash

set -e

source /opt/installers/install_base.sh

function install_asnwrapper {
    local url="https://zhilu.bj.bcebos.com/asn_wrapper.tar.gz"
    local sha256="1c5d3f055f33c77ef3a39b1a9807a228c33c4faf58f692f5d3120ce292bcb20c"
    [ -z "${url}" ] && exit
    pushd /opt/ >/dev/null
        wget -c ${url}
        if check_sha256 "asn_wrapper.tar.gz" "${sha256}"; then
            tar -zxf asn_wrapper.tar.gz
            python3 -m pip install --timeout 30 --no-cache-dir protobuf
        else
            echo "check_sha256 failed: asn_wrapper.tar.gz"
        fi
        rm asn_wrapper.tar.gz
    popd >/dev/null
}

function main {
    install_asnwrapper
}

main "$@"
