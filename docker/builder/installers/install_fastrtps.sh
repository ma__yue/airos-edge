#!/usr/bin/env bash

set -e

source /opt/installers/install_base.sh

function install_fastrtps {
    local url="https://zhilu.bj.bcebos.com/fast-rtps.tar.gz"
    local sha256="fe21fe6f5c80d05a801f29ca86f8fcaa4f0a471994ae9cec9c78dd748f67d094"
    [ -z "${url}" ] && exit
    pushd /opt/ >/dev/null
        wget -c ${url}
        if check_sha256 "fast-rtps.tar.gz" "${sha256}"; then
            tar -zxf fast-rtps.tar.gz
            python3 -m pip install --timeout 30 --no-cache-dir protobuf 
        else
            echo "check_sha256 failed: fast-rtps.tar.gz"
        fi
        rm fast-rtps.tar.gz
    popd >/dev/null
}

function main {
    install_fastrtps
}

main "$@"