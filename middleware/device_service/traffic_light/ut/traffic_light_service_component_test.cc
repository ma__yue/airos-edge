/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <cassert>
#include <iostream>
#include <string>

#include "gtest/gtest.h"

#define private public
#include "middleware/device_service/traffic_light/traffic_light_service_component.h"

namespace os {
namespace v2x {
namespace service {

class TrafficLightServiceTest : public ::testing::Test {
 public:
  TrafficLightServiceTest() {}
  virtual ~TrafficLightServiceTest() {}
  void SetUp() override {
    airos::middleware::AirRuntimeInit("traffic_light_service_test");
    const std::string file =
        "/airos/middleware/device_service/traffic_light/ut/testdata/"
        "traffic_light_device.pb.txt";
    if (!apollo::cyber::common::GetProtoFromFile(file, &real_device_data_)) {
      AERROR << "read file conf failed!";
      return;
    }
  }

 protected:
  AIROS_COMPONENT_CLASS_NAME(TrafficLightServiceComponent)
  traffic_light_service_component_;
  os::v2x::device::TrafficLightBaseData real_device_data_;
};

TEST_F(TrafficLightServiceTest, test_service) {
  std::shared_ptr<os::v2x::device::TrafficLightBaseData> device_data = nullptr;
  std::shared_ptr<os::v2x::TrafficLightServiceData> service_data = nullptr;
  EXPECT_FALSE(traffic_light_service_component_.TrafficLightDataPb2ServicePb(
      device_data, service_data));

  service_data = std::make_shared<os::v2x::TrafficLightServiceData>();
  device_data = std::make_shared<os::v2x::device::TrafficLightBaseData>(
      real_device_data_);
  EXPECT_TRUE(traffic_light_service_component_.TrafficLightDataPb2ServicePb(
      device_data, service_data));
  AINFO << service_data->DebugString();
}

}  // namespace service
}  // namespace v2x
}  // namespace os