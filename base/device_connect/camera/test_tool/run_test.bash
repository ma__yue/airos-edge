CUR_DIR="$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd -P)"

pushd "${CUR_DIR}/../../" > /dev/null
    source setup.bash
popd > /dev/null

./camera_test_tool conf/camera1.yaml conf/camera2.yaml conf/camera3.yaml