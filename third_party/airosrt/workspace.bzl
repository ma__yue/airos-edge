def clean_dep(dep):
    return str(Label(dep))

def repo():
    native.new_local_repository(
        name = "airosrt",
        build_file = clean_dep("//third_party/airosrt:airosrt.BUILD"),
        path = "/opt/airosrt"
    )