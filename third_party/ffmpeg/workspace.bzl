def clean_dep(dep):
    return str(Label(dep))

def repo():
    native.new_local_repository(
        name = "ffmpeg",
        build_file = clean_dep("//third_party/ffmpeg:ffmpeg.BUILD"),
        path = "/opt/ffmpeg"
    )