/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include <Eigen/Core>
#include <Eigen/Dense>
#include <memory>
#include <string>
#include <vector>

#include "base/blob/blob.h"
#include "air_service/modules/perception-camera/algorithm/interface/perception_frame.h"

namespace airos {
namespace perception {
namespace visualization {

class CameraPerceptionVizMessage {
 public:
  CameraPerceptionVizMessage() = default;
  ~CameraPerceptionVizMessage() = default;

  std::string GetTypeName() const { return "CameraPerceptionVizMessage"; }

  CameraPerceptionVizMessage(
      const std::string& camera_name, const double msg_timestamp,
      const Eigen::Matrix4d& pose_camera_to_world,
      const Eigen::Matrix3f intrinsic_params,
      const std::shared_ptr<airos::base::Blob<uint8_t>>& image_blob,
      const std::vector<camera::ObjectInfo>& camera_objects)
      : camera_name_(camera_name),
        msg_timestamp_(msg_timestamp),
        pose_camera_to_world_(pose_camera_to_world),
        intrinsic_params_(intrinsic_params),
        image_blob_(image_blob) {
    camera_objects_.clear();
    for (const auto& obj : camera_objects) {
      camera_objects_.push_back(obj);
    }
  }

 public:
  std::string camera_name_;
  double msg_timestamp_ = 0.0;
  Eigen::Matrix4d pose_camera_to_world_;
  Eigen::Matrix3f intrinsic_params_;
  // std::shared_ptr<base::Image8U> image;
  std::shared_ptr<airos::base::Blob<uint8_t>> image_blob_;

  std::vector<camera::ObjectInfo> camera_objects_;  // TODO
};

}  // namespace visualization
}  // namespace perception
}  // namespace airos
